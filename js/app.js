const data={
    "even": {
        "basic_price": "&dollar; 19.99",
        "pro_price": "&dollar; 24.99",
        "master_price": "&dollar; 39.99"
    },
    "odd": {
        "basic_price": "&dollar; 199.99",
        "pro_price": "&dollar; 249.99",
        "master_price": "&dollar; 399.99"
    }
}

// selectors
const toggleSlider = document.querySelector(".slider");
const basic_price = document.querySelector(".basic_price");
const pro_price = document.querySelector(".pro_price");
const master_price = document.querySelector(".master_price");
// event listeners
var c=0
toggleSlider.addEventListener("click", () => {
    toggleState(c)
    c += 1;
}); 


// functions
function toggleState(c) {
    if (c % 2 == 0) {
        basic_price.innerHTML=data.even.basic_price
        pro_price.innerHTML=data.even.pro_price
        master_price.innerHTML=data.even.master_price
    }
    else {
        basic_price.innerHTML=data.odd.basic_price
        pro_price.innerHTML = data.odd.pro_price
        master_price.innerHTML=data.odd.master_price
    }
}